#include "libFatura.h"

#define TAMFORNECEDOR 10

typedef struct nFornecedor{
	char razaoSocial[20];
	char cnpj[20];
	char dataUltimaEntrega[11];
	fatura faturasEmAberto[5];
}fornecedor;

fornecedor* alocarVFornecedor(int tam);

fornecedor* realocarVFornecedor(fornecedor* fornecedores);

fornecedor* inserirFornecedor(fornecedor* fornecedores, fatura* faturas);

fornecedor* removerFornecedor(fornecedor* fornecedores);

void imprimeFornecedor(fornecedor* fornecedores);

void imprimeFaturasEmAberto(fornecedor fornecedor);

int sizeofFornecedores(fornecedor* fornecedores);


/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFornecedor(fornecedor* fornecedores);

fornecedor* recuperarDadosFornecedor();

void salvarContagemFornecedor(fornecedor* fornecedores, FILE* file);

void recuperarContagemFornecedor(FILE* file);

void removerArquivoFornecedor();