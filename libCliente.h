#include "libFuncionario.h"
#define TAMINICIALCLIENTE 5 //Tamanho inicial que o vetor terá ao ser alocado;

typedef struct nCliente {
  char nome[20];
  char sobrenome[50];
  char dataUltimaCompra[11];
  int estado; //0 para devendo, 1 para ok;
  funcionario ultimosVendedores[5];
} cliente;

cliente* alocarVCliente(int tam);

cliente* realocarVCliente(cliente* clientes);

cliente* inserirCliente(cliente* clientes, funcionario* funcionarios);

cliente* removerCliente(cliente* clientes);

void imprimeCliente(cliente* clientes);

void imprimeUltimosFuncionarios(cliente cliente);

int sizeofClientes(cliente* clientes);




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarContagemCliente(cliente* clientes, FILE* file);

void recuperarContagemCliente(FILE* file);

void salvarDadosCliente(cliente* clientes);

cliente* recuperarDadosCliente();

void removerArquivoCliente();

