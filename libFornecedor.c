#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libFornecedor.h"
#include "libSistema.h"

#define NOMEARQUIVOFORNECEDOR "fornecedores.bin"

int countVFornecedor = 10;

fornecedor* alocarVFornecedor(int tam){
	fornecedor* fornecedores = (fornecedor*) malloc(tam * sizeof(fornecedor));

	//Seta o campo cnpj de todos os fornecedores para "" (para posterior comparação ao procurar espaços vazios no vetor)
	int i; 
	for(i = 0; i < tam; i++){
		strcpy(fornecedores[i].cnpj, "");
	}

	return fornecedores;
}

fornecedor* inserirFornecedor(fornecedor* fornecedores, fatura* faturas){

	int countFornecedores = sizeofFornecedores(fornecedores);

	if(countFornecedores == TAMFORNECEDOR){
		printf("\nNumero maximo de fornecedores atingidos!\n");
		printf("Remova algum fornecedor antigo para adicionar um novo!\n");
		return fornecedores;	
	}

	char razaoSocial[20];
	char cnpj[20];
	char dataUltimaEntrega[11];

	//Entrada de dados
	printf("Insira a Razao Social: ");
	scanf("%s", razaoSocial);
	printf("Insira o CNPJ: ");
	scanf("%s", cnpj);
	printf("Insira a data da ultima entrega: ");
	scanf("%s", dataUltimaEntrega);

	//Verifica se já possui algum fornecedor com este cnpj, se sim reinicia o processo.
	int i;
	for(i = 0; i < countFornecedores; i++){
		if(strcmp(fornecedores[i].cnpj, cnpj) == 0){
			printf("\nJa existe um fornecedor com este CNPJ, tente novamente!\n\n");
			return inserirFornecedor(fornecedores, faturas); //Chamada recursiva, irá chamar novamente a função e irá retornar o resultado.
			break;
		}
	}

	//Registrar faturas em aberto.
	if(sizeofFaturas(faturas) > 0){
		int id;
		printf("\nDigite os ids dos faturas em aberto, ou 0 para finalizar. \n");
		int i;
		for(i = 0; i < 5; i++){
			printf("Fatura %d: ", i+1);
			scanf("%d", &id);

			if(id == 0){
				break;
			}	

			int j, found = 0;
			for(j = 0; j < sizeofFaturas(faturas); j++){
				if(faturas[j].id == id){
					found = 1;
					strcpy(fornecedores[countFornecedores].faturasEmAberto[i].data, faturas[j].data);
					fornecedores[countFornecedores].faturasEmAberto[i].id = faturas[j].id;
				}
			}

			if(found == 0){
				printf("Fatura não encontrada, tente novamente.\n");
				i--;
			}
		}

		for(;i < 5; i++){
			fornecedores[countFornecedores].faturasEmAberto[i].id = -1;
		}
	}else{
		int i;		
		for(i = 0;i < 5; i++){
			fornecedores[countFornecedores].faturasEmAberto[i].id = -1;
		}
	}

	//Se passou pelo teste então irá criar o novo fornecedor.
	strcpy(fornecedores[countFornecedores].razaoSocial, razaoSocial);
	strcpy(fornecedores[countFornecedores].cnpj, cnpj);
	strcpy(fornecedores[countFornecedores].dataUltimaEntrega, dataUltimaEntrega);

	return fornecedores;
}

fornecedor* removerFornecedor(fornecedor* fornecedores){	

	int countFornecedores = sizeofFornecedores(fornecedores);

	char cnpj[20]; 
	int pos = -1; //Posição do fornecedor no vetor, inicia com -1 para o caso de não encontrar o fornecedor.

	//Entrada de cnpj para procurar o fornecedor no vetor;
	printf("CNPJ: ");
	scanf("%s", cnpj);

	//Irá procurar no vetor o cnpj informado.
	int i;
	for(i = 0; i < countFornecedores; i++){
		if(strcmp(fornecedores[i].cnpj, cnpj) == 0){
			pos = i;
			break;
		}
	}

	//Caso encontre o "pos" será > -1
	if(pos > -1){
		//Cria novo vetor para adicionar somente os que não foram excluidos;
		fornecedor* fornecedores2 = alocarVFornecedor(TAMFORNECEDOR);

		//adiciona os fornecedores do vetor "fornecedores" no novo vetor "fornecedores2"
		int i;
		for(i = 0; i < (countFornecedores-1); i++){
			if(i >= pos){
				fornecedores2[i] = fornecedores[i+1];
			}else{
				fornecedores2[i] = fornecedores[i];
			}
		}

		//É retornado o novo vetor já com o fornecedor excluido.
		return fornecedores2;
	}else{
		//Fornecedor não encontrado, então irá retornar o mesmo vetor sem alterações.
		printf("\nFornecedor não encontrado. Tente novamente.");
		return fornecedores;
	}
}

void imprimeFornecedor(fornecedor* fornecedores){

	if(sizeofFornecedores(fornecedores)==0){
		printf("\nNão há fornecedores cadastrados!!!\n\n");
		return;
	}

	printf("\n_____________________________________________________________________________________________________\n");
	printf("|QTD  | RAZAO SOCIAL        | CNPJ                | DATA ULTIMA ENTREGA |  FATURAS EM ABERTO         |\n");
	printf("|_____|_____________________|_____________________|_____________________|____________________________|\n");
  	//Percorre todo o vetor de fornecedores exibindo suas informações
	int i;
	for(i = 0; i < sizeofFornecedores(fornecedores); i++){
		if(&fornecedores[i]){
			printf("| %-4d| %-20s| %-20s| %-20s|", i+1, fornecedores[i].razaoSocial, fornecedores[i].cnpj, fornecedores[i].dataUltimaEntrega);
			imprimeFaturasEmAberto(fornecedores[i]);
		}
	printf("\n|_____|_____________________|_____________________|_____________________|____________________________|\n");
	}
}

void imprimeFaturasEmAberto(fornecedor fornecedor){	
	int i;
	for(i = 0; i < 5; i++){
		if(fornecedor.faturasEmAberto[i].id > -1){
			if(i == 0){
				printf(" %4d - %-20s|", fornecedor.faturasEmAberto[i].id, fornecedor.faturasEmAberto[i].data);
			}else{
				printf("\n|     |                     |                     |                     | %4d - %-20s|", fornecedor.faturasEmAberto[i].id, fornecedor.faturasEmAberto[i].data);
			}
		}else if(i==0){
			printf(" %-27s|", "");			
		}
	}
}

//Calcula o tamanho do vetor(Não calcula a memória alocada, calcula a quantidade de fornecedores incluidas no vetor).
int sizeofFornecedores(fornecedor* fornecedores){
	int count = 0;

	//Percorre todo o vetor até que encontre um espaço do vetor que não esteja ocupado por um fatura.(Que esteja vazio)
	int i;
	for(i = 0; i < TAMFORNECEDOR; i++){
		if(strcmp(fornecedores[count].cnpj, "") == 0){
			break;
		}
		count++;
	}
	
	return count; 
}




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFornecedor(fornecedor* fornecedores){
	criarArquivo(NOMEARQUIVOFORNECEDOR);

	FILE* file = abrirArquivo(NOMEARQUIVOFORNECEDOR);

	fseek ( file, 0, SEEK_SET);

	int i;
	for(i = 0; i < sizeofFornecedores(fornecedores); i++){
		fwrite(&fornecedores[i], sizeof(fornecedor), 1, file);
	}

	fclose(file);
}

fornecedor* recuperarDadosFornecedor(){
	fornecedor* fornecedores = alocarVFornecedor(countVFornecedor);

	FILE* file = abrirArquivo(NOMEARQUIVOFORNECEDOR);

	if(file){
		fseek ( file, 0, SEEK_SET);
		int i;
		for(i = 0; i < countVFornecedor; i++){
			fread(&fornecedores[i], sizeof(fornecedor), 1, file);
		}
		fclose(file);
	}

	return fornecedores;
}

void salvarContagemFornecedor(fornecedor* fornecedores, FILE* file){	
	int position = 1;
  	int qtd = sizeofFornecedores(fornecedores);
  	fseek ( file, position * sizeof(int), SEEK_SET);
    fwrite (&qtd, sizeof (int), 1, file);
}

void recuperarContagemFornecedor(FILE* file){
	int countRead = 0;
	int position = 1;
	fseek ( file, position * sizeof(int), SEEK_SET);

	fread (&countRead, sizeof(int), 1, file);
  	if(countRead != 0){
  		countVFornecedor = countRead;
  	}
	printf ("Fornecedores: %d\n", countVFornecedor);  
}

void removerArquivoFornecedor(){
	remove(NOMEARQUIVOFORNECEDOR);
}

