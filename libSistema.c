#include <stdlib.h>
#include <stdio.h>
#include "libSistema.h"

void limpar(){
	if(strcmp(OS, "Linux") == 0){
		system("clear");
	}else if(strcmp(OS, "Windows") == 0){
		system("cls");
	}
}

FILE* criarArquivo(char* nome){	
  FILE * pFile = NULL;
  
  if ((pFile = fopen(nome, "wb"))) {
	  return pFile;
  } else {
      printf ("Erro na criação de arquivo \n");
      return NULL;
  }
}

FILE* abrirArquivo(char* nome){	
  FILE * pFile = NULL;
  
  if ((pFile = fopen(nome, "rb+"))) {
	  return pFile;
  } else {
      printf ("Erro na abertura de arquivo \n");
      return NULL;
  }
}
