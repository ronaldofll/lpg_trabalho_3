#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libCliente.h"
#include "libSistema.h"

#define NOMEARQUIVOCLIENTE "clientes.bin"

int countVCliente = 5; //Quantidade disponível no vetor de clientes para alocação de clientes.

cliente* alocarVCliente(int tam){

	cliente* v = (cliente*) malloc(tam * sizeof(cliente));

	//Seta o campo nome de todos os clientes para "" (para posterior comparação ao procurar espaços vazios no vetor)
	int i;
	for(i = 0; i < tam; i++){
		strcpy(v[i].nome, "");
	}

	countVCliente = tam;

	return v;
}

cliente* realocarVCliente(cliente* clientes){
	countVCliente += 5; //Atualiza a quantidade disponível no vetor de clientes.
	clientes = (cliente*) realloc(clientes, countVCliente * sizeof(cliente)); //Realoca o vetor clientes com o novo tamanho.

	//Seta o campo nome para "" dos novos espaços no vetor.
	int i;
	for(i = (countVCliente-5); i < countVCliente; i++){
		strcpy(clientes[i+1].nome, "");
	}
	return clientes;
}

cliente* inserirCliente(cliente* clientes, funcionario* funcionarios){

	//Verifica a quantidade atual de clientes no vetor.
	int countClientes = sizeofClientes(clientes);

	//Verifica se precisa alocar mais memória no vetor para incluir o novo cliente.
	if((countVCliente - countClientes) == 0){
		clientes = realocarVCliente(clientes);	
	}
	
	char nome[20]; 
	char sobrenome[50];
	char dataUltimaCompra[50];
	int devedor;

	//Entrada de informações para criar novo cliente.
	printf("Nome: ");
	scanf("%s", nome);
	printf("Sobrenome: ");
	scanf("%s", sobrenome);
	printf("Data da última compra: ");
	scanf("%s", dataUltimaCompra);

	for(;;){
		printf("Devedor? (0 para sim, 1 para não): ");
		scanf("%d", &devedor);
		if(devedor == 0 || devedor == 1){
			break;
		}
	}
	
	//Registrar ultimos vendedores para este cliente.
	if(sizeofFuncionarios(funcionarios) > 0){
		int id;
		printf("\nDigite os ids dos ultimos vendedores, ou 0 para finalizar. \n");
		int i;
		for(i = 0; i < 5; i++){
			printf("Vendedor %d: ", i+1);
			scanf("%d", &id);

			if(id == 0){
				break;
			}	

			int j, found = 0;
			for(j = 0; j < sizeofFuncionarios(funcionarios); j++){
				if(funcionarios[j].id == id){
					found = 1;
					strcpy(clientes[countClientes].ultimosVendedores[i].nome, funcionarios[j].nome);
					clientes[countClientes].ultimosVendedores[i].id = funcionarios[j].id;
				}
			}

			if(found == 0){
				printf("Vendedor não encontrado, tente novamente.\n");
				i--;
			}
		}

		for(;i < 5; i++){
			clientes[countClientes].ultimosVendedores[i].id = -1;
		}
	}else{
		int i;		
		for(i = 0;i < 5; i++){
			clientes[countClientes].ultimosVendedores[i].id = -1;
		}
	}

	//Copia as informações passadas para o novo cliente.
	strcpy(clientes[countClientes].nome, nome);
	strcpy(clientes[countClientes].sobrenome, sobrenome);
	strcpy(clientes[countClientes].dataUltimaCompra, dataUltimaCompra);
	clientes[countClientes].estado = devedor; 

	return clientes;
}

cliente* removerCliente(cliente* clientes){
	
	char nome[20]; 
	char sobrenome[50];
	int pos = -1; //Posição do cliente no vetor, inicia com -1 para o caso de não encontrar o cliente.

	//Entrada de nome e sobrenome para procurar o cliente no vetor;
	printf("Nome: ");
	scanf("%s", nome);
	printf("Sobrenome: ");
	scanf("%s", sobrenome);

	//Irá procurar no vetor o nome e sobrenome digitado.
	int i;
	for(i = 0; i < sizeofClientes(clientes); i++){
		if(strcmp(clientes[i].nome, nome) == 0 && strcmp(clientes[i].sobrenome, sobrenome) == 0){
			pos = i;
			break;
		}
	}

	//Caso encontre o "pos" será > -1
	if(pos > -1){
		//Cria novo vetor para adicionar somente os que não foram excluidos;
		cliente* clientes2 = alocarVCliente(countVCliente);

		//adiciona os clientes do vetor "clientes" no novo vetor "clientes2"
		int i;
		for(i = 0; i < (sizeofClientes(clientes)-1); i++){
			if(i >= pos){
				clientes2[i] = clientes[i+1];
			}else{
				clientes2[i] = clientes[i];
			}
		}

		//É retornado o novo vetor já com o cliente excluido.
		return clientes2;
	}else{
		//Cliente não encontrado, então irá retornar o mesmo vetor sem alterações.
		printf("\nCliente não encontrado. Tente novamente.");
		return clientes;
	}
}

void imprimeCliente(cliente *clientes){

	if(sizeofClientes(clientes)==0){
		printf("\nNão há clientes cadastrados!!!\n\n");
		return;
	}

	printf("\n_____________________________________________________________________________________________________________________________________________\n");
	printf("|QTD  | NOME                | SOBRENOME                                         | DATA ULTIMA COMPRA | ESTADO  | ULTIMOS VENDEDORES         |\n");
	printf("|_____|_____________________|___________________________________________________|____________________|_________|____________________________|\n");
  	//Percorre todo o vetor de clientes exibindo suas informações
	int i;
	for(i = 0; i < sizeofClientes(clientes); i++){
		if(&clientes[i]){
			char* estado = (clientes[i].estado) ? "OK" : "DEVEDOR";
			//printf("| %-4d| %-20s| %-50s| %-19s| %-8s| %-20s|", i+1, clientes[i].nome, clientes[i].sobrenome, clientes[i].dataUltimaCompra, estado, "");
			printf("| %-4d| %-20s| %-50s| %-19s| %-8s|", i+1, clientes[i].nome, clientes[i].sobrenome, clientes[i].dataUltimaCompra, estado);
			imprimeUltimosFuncionarios(clientes[i]);
		}
	printf("\n|_____|_____________________|___________________________________________________|____________________|_________|____________________________|\n");
	}
}

void imprimeUltimosFuncionarios(cliente cliente){
	int i;
	for(i = 0; i < 5; i++){
		if(cliente.ultimosVendedores[i].id > -1){
			if(i == 0){
				printf(" %4d - %-20s|", cliente.ultimosVendedores[i].id, cliente.ultimosVendedores[i].nome);
			}else{
				printf("\n|     |                     |                                                   |                    |         | %4d - %-20s|", cliente.ultimosVendedores[i].id, cliente.ultimosVendedores[i].nome);
			}
		}else if(i==0){
			printf(" %-27s|", "");			
		}
	}
}

//Calcula o tamanho do vetor(Não calcula a memória alocada, calcula a quantidade de clientes incluidas no vetor).
int sizeofClientes(cliente* clientes){
	int count = 0;

	//Percorre todo o vetor até que encontre um espaço do vetor que não esteja ocupado por um fatura.(Que estejavazio)
	int i;
	for(i = 0; i < countVCliente; i++){
		if(strcmp(clientes[count].nome, "") == 0){
			break;
		}
		count++;
	}

	return count;
}




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/
void salvarDadosCliente(cliente* clientes){
	criarArquivo(NOMEARQUIVOCLIENTE);

	FILE* file = abrirArquivo(NOMEARQUIVOCLIENTE);

	fseek ( file, 0, SEEK_SET);

	int i;
	for(i = 0; i < sizeofClientes(clientes); i++){
		fwrite(&clientes[i], sizeof(cliente), 1, file);
	}

	fclose(file);
}

cliente* recuperarDadosCliente(){
	cliente* clientes = alocarVCliente(countVCliente);

	FILE* file = abrirArquivo(NOMEARQUIVOCLIENTE);

	if(file){
		fseek ( file, 0, SEEK_SET);
		int i;
		for(i = 0; i < countVCliente; i++){
			fread(&clientes[i], sizeof(cliente), 1, file);
		}
		fclose(file);
	}

	return clientes;
}

void salvarContagemCliente(cliente* clientes, FILE* file){
  	fseek ( file, 0, SEEK_SET);
  	int qtd = sizeofClientes(clientes);
   	fwrite (&qtd, sizeof (int), 1, file);
}

void recuperarContagemCliente(FILE* file){
	int countRead = 0;

	fseek ( file, 0, SEEK_SET);
	fread (&countRead, sizeof(int), 1, file);

	if(countRead != 0){
		countVCliente = countRead;
	}
	printf ("Clientes: %d\n", countVCliente);  
}