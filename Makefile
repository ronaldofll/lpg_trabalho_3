CC = gcc
CFLAGS = -g -Wall

exec: main.c
	$(CC) $(CFLAGS) main.c libCliente.c libFuncionario.c libFornecedor.c libFatura.c libSistema.c -o main.e

clean :
	rm *.e
	rm *.bin
