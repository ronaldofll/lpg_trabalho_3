#define TAMINICIALFATURA 5

typedef struct nFatura{
	int id;	
	char data[11];
}fatura;

fatura* alocarVFatura(int tam);

fatura* realocarVFatura(fatura* faturas);

fatura* inserirFatura(fatura* faturas);

fatura* removerFatura(fatura* faturas);

void imprimeFatura(fatura* faturas);

int sizeofFaturas(fatura* faturas);



/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFatura(fatura* faturas);

fatura* recuperarDadosFatura();

void salvarContagemFatura(fatura* faturas, FILE* file);

void recuperarContagemFatura(FILE* file);

void removerArquivoCliente();