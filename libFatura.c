#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libFatura.h"
#include "libSistema.h"

#define NOMEARQUIVOFATURA "faturas.bin"

int countVFatura = 5; //Quantidade disponível no vetor de fatura para alocação de faturas.

fatura* alocarVFatura(int tam){

	//criarArquivo(NOMEARQUIVOFATURA);

	fatura* faturas = (fatura*) malloc(tam * sizeof(fatura));

	int i;
	for(i = 0; i < tam; i++){
		faturas[i].id = -1; //Seta todos os ids para -1 para não ficar como 0
	}

	countVFatura = tam;

	return faturas;
}

fatura* realocarVFatura(fatura* faturas){	
	countVFatura += 5; //Atualiza a quantidade disponível no vetor de faturas.
	faturas = (fatura*) realloc(faturas, countVFatura * sizeof(fatura)); //Realoca o vetor faturas com o novo tamanho.
	int i;
	for(i = (countVFatura-5); i < countVFatura; i++){
		faturas[i+1].id = -1; //Seta todos os ids dos novos espaços no veotr para -1 para não ficar como 0
	}

	return faturas;
}

fatura* inserirFatura(fatura* faturas){

	int countFaturas = sizeofFaturas(faturas);

	if((countVFatura - countFaturas) == 0){
		faturas = realocarVFatura(faturas);	
	}

	char data[11];
	int id;

	//Entrada de dados
	printf("Insira o id: ");
	scanf("%d", &id);
	printf("Insira a data: ");
	scanf("%s", data);

	//Verifica se já possui algum fatura com este id, se sim reinicia o processo.
	int i;
	for(i = 0; i < countFaturas; i++){
		if(faturas[i].id == id){
			printf("\nEste id já está sendo utilizado por outro fatura, tente novamente!\n\n");
			return inserirFatura(faturas); //Chamada recursiva, irá chamar novamente a função e irá retornar o resultado.
			break;
		}
	}

	//Se passou pelo teste então irá criar o novo fatura.
	strcpy(faturas[countFaturas].data, data);
	faturas[countFaturas].id = id;

	return faturas;
}

fatura* removerFatura(fatura* faturas){	
	
	int id; 
	int pos = -1; //Posição do fatura no vetor, inicia com -1 para o caso de não encontrar o fatura.

	//Entrada de id para procurar o fatura no vetor;
	printf("ID: ");
	scanf("%d", &id);

	//Irá procurar no vetor o nome e sobrenome digitado.
	int i;
	for(i = 0; i < sizeofFaturas(faturas); i++){
		if(faturas[i].id == id){
			pos = i;
			break;
		}
	}

	//Caso encontre o "pos" será > -1
	if(pos > -1){
		//Cria novo vetor para adicionar somente os que não foram excluidos;
		fatura* faturas2 = alocarVFatura(countVFatura);

		//adiciona os faturas do vetor "faturas" no novo vetor "faturas2"
		int i;
		for(i = 0; i < (sizeofFaturas(faturas)-1); i++){
			if(i >= pos){
				faturas2[i] = faturas[i+1];
			}else{
				faturas2[i] = faturas[i];
			}
		}

		//É retornado o novo vetor já com o fatura excluido.
		return faturas2;
	}else{
		//Fatura não encontrado, então irá retornar o mesmo vetor sem alterações.
		printf("\nFatura não encontrado. Tente novamente.");
		return faturas;
	}
}

void imprimeFatura(fatura* faturas){

	if(sizeofFaturas(faturas)==0){
		printf("\nNão há faturas cadastradas!!!\n\n");
		return;
	}

	printf("\n_________________________________________\n");
	printf("|QTD  | ID        | DATA                |\n");
	printf("|_____|___________|_____________________|\n");
  	//Percorre todo o vetor de faturas exibindo suas informações
	int i;
	for(i = 0; i < sizeofFaturas(faturas); i++){
		if(&faturas[i]){
			printf("| %-4d| %-10d| %-20s|", i+1, faturas[i].id, faturas[i].data);
		}
	printf("\n|_____|___________|_____________________|\n");
	}
}

//Calcula o tamanho do vetor(Não calcula a memória alocada, calcula a quantidade de faturas incluidas no vetor).
int sizeofFaturas(fatura* faturas){
	int count = 0;

	//Percorre todo o vetor até que encontre um espaço do vetor que não esteja ocupado por um fatura.(Que esteja nulo ou vazio)
	int i;
	for(i = 0; i < countVFatura; i++){
		if(faturas[count].id == -1){
			break;
		}
		count++;
	}
	
	return count;
}




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFatura(fatura* fatura){
	criarArquivo(NOMEARQUIVOFATURA);

	FILE* file = abrirArquivo(NOMEARQUIVOFATURA);

	fseek ( file, 0, SEEK_SET);

	int i;
	for(i = 0; i < sizeofFaturas(fatura); i++){
		fwrite(&fatura[i], sizeof(fatura), 1, file);
	}

	fclose(file);
}

fatura* recuperarDadosFatura(){
	fatura* fatura = alocarVFatura(countVFatura);

	FILE* file = abrirArquivo(NOMEARQUIVOFATURA);

	if(file){
		fseek ( file, 0, SEEK_SET);
		int i;
		for(i = 0; i < countVFatura; i++){
			fread(&fatura[i], sizeof(fatura), 1, file);
		}
		fclose(file);
	}

	return fatura;
}

void salvarContagemFatura(fatura* faturas, FILE* file){
	int position = 2;
  	fseek ( file, (position * sizeof(int)), SEEK_SET);
    fwrite (&countVFatura, sizeof (int), 1, file);
}

void recuperarContagemFatura(FILE* file){
	int countRead = 0;

	int position = 2;
  	fseek ( file, position * sizeof(int), SEEK_SET);
  	fread (&countRead, sizeof(int), 1, file);

  	if(countRead != 0){
  		countVFatura = countRead;
  	}
  	printf ("Faturas: %d\n", countVFatura);  
}

void removerArquivoFatura(){
	remove(NOMEARQUIVOFATURA);
}
