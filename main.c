
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "libCliente.h"
#include "libFornecedor.h"
#include "libSistema.h"

/**Exibir Menu*/
int menuPrincipal();
void incluir();
void excluir();
void listar();
void salvarContagem();
void salvarDados();
void recuperarContagem();
void recuperarDados();

cliente* clientes;
fornecedor* fornecedores;
funcionario* funcionarios;
fatura* faturas;

int main(){

	recuperarContagem();
	recuperarDados();

	menuPrincipal();

	salvarContagem();
	salvarDados();
	
	return 0;
	
}

void salvarDados(){
	salvarDadosCliente(clientes);
	salvarDadosFornecedor(fornecedores);
	salvarDadosFuncionario(funcionarios);
	salvarDadosFatura(faturas);

}

void recuperarDados(){
	clientes = recuperarDadosCliente();
	fornecedores = recuperarDadosFornecedor();
	funcionarios = recuperarDadosFuncionario();
	faturas = recuperarDadosFatura();
}

void salvarContagem(){
	criarArquivo("count.bin");

	FILE* file = abrirArquivo("count.bin");

	if(file){
		salvarContagemCliente(clientes,file);
		salvarContagemFuncionario(funcionarios, file);
		salvarContagemFatura(faturas, file);
  		fclose(file);
	}

}

void recuperarContagem(){
	FILE* file = abrirArquivo("count.bin");

	if(file){
		recuperarContagemCliente(file);
		recuperarContagemFuncionario(file);
		recuperarContagemFatura(file);
  		fclose(file);
	}
}

void removerArquivos(){
	removerArquivoCliente();
	removerArquivoFornecedor();
	removerArquivoFuncionario();
	removerArquivoFatura();
}

int menuPrincipal(){

	limpar();
	printf("\n\n.....:::::Menu Principal:::::.....\n");
	printf("1 - Incluir\n");
	printf("2 - Excluir\n");
	printf("3 - Listar\n");
	printf("4 - Remover arquivos\n");
	printf("0 - Sair\n");
	int opc = 0;
	printf("Digite uma das opcoes: ");
	scanf("%d", &opc);
	getchar(); //Evitar que o próximo scanf use a última opc digitada

	switch(opc){
		case 0:
			return opc;
		case 1:
			incluir();
			break;
		case 2:
			excluir();
			break;
		case 3:
			listar();
			break;
		case 4:
			removerArquivos();
			break;
		default:
			printf("Opcao invalida, tente novamente.");
			menuPrincipal();
			break;
	}

	return opc;
}

void incluir(){
	int opc;

	limpar();
	printf("\n\n.....:::::Menu Incluir:::::.....\n");
	printf("1 - Cliente\n");
	printf("2 - Fornecedor\n");
	printf("3 - Funcionario\n");
	printf("4 - Fatura\n");
	printf("0 - Voltar\n");
	printf("Digite uma das opcoes: ");
	scanf("%d", &opc);
	getchar(); //Evitar que o próximo scanf utilize a última opc digitada

	switch(opc){
		case 0:
			menuPrincipal();
			break;
		case 1: //Registro de Clientes
			clientes = inserirCliente(clientes, funcionarios);
			break;
		case 2:
			fornecedores = inserirFornecedor(fornecedores, faturas);
			break;
		case 3:
			funcionarios = inserirFuncionario(funcionarios);
			break;
		case 4:
			faturas = inserirFatura(faturas);
			break;
		default:
			printf("Opcao invalida, tente novamente.");
			incluir();
			break;
	}

	menuPrincipal();
}

void excluir(){

	limpar();
	printf("\n\n.....:::::Menu Excluir:::::.....\n");
	int opc;
	printf("1 - Cliente\n");
	printf("2 - Fornecedor\n");
	printf("3 - Funcionario\n");
	printf("4 - Fatura\n");
	printf("0 - Voltar\n");
	printf("Digite uma das opcoes: ");
	scanf("%d", &opc);
	getchar(); //Evitar que o próximo scanf use a última opc digitada

	switch(opc){
		case 0:
			menuPrincipal();
			break;
		case 1:
			clientes = removerCliente(clientes);			
			break;
		case 2:
			fornecedores = removerFornecedor(fornecedores);			
			break;
		case 3:
			funcionarios = removerFuncionario(funcionarios);			
			break;
		case 4:
			faturas = removerFatura(faturas);			
			break;
		default:
			printf("Opcao invalida, tente novamente.");
			excluir();
			break;
	}

	menuPrincipal();
}

void listar(){

	limpar();
	printf("\n\n.....:::::Menu Listar:::::.....\n");
	printf("1 - Cliente\n");
	printf("2 - Fornecedor\n");
	printf("3 - Funcionario\n");
	printf("4 - Fatura\n");
	printf("0 - Voltar\n");
	int opc = -1;
	printf("Digite uma das opcoes: ");
	scanf("%d", &opc);
	getchar(); //Evitar que o próximo scanf use a última opc digitada

	switch(opc){
		case 0:
			menuPrincipal();
		case 1:	
			imprimeCliente(clientes);
			break;
		case 2:
			imprimeFornecedor(fornecedores);
			break;
		case 3:		
			imprimeFuncionario(funcionarios);
			break;
		case 4:
			imprimeFatura(faturas);
			break;

		default:
			printf("Opcao invalida, tente novamente.");
			listar();
	}

	printf("\nPressione qualquer para continuar...");
	getchar();
	menuPrincipal();
}
