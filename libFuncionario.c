#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libFuncionario.h"
#include "libSistema.h"

#define NOMEARQUIVOFUNCIONARIO "funcionarios.bin"

int countVFuncionario = 5; //Quantidade disponível no vetor de funcionarios para alocação de funcionarios.

funcionario* alocarVFuncionario(int tam){

	funcionario* funcionarios = (funcionario*) malloc(tam * sizeof(funcionario));

	//Seta o campo id de todos os funcionarios para "" (para posterior comparação ao procurar espaços vazios no vetor)
	int i;
	for(i = 0; i < tam; i++){
		funcionarios[i].id = -1; //Seta todos os ids para -1 para não ficar como 0
	}

	countVFuncionario = tam;

	return funcionarios;
}

funcionario* realocarVFuncionario(funcionario* funcionarios){	
	countVFuncionario += 5; //Atualiza a quantidade disponível no vetor de funcionarios.
	funcionarios = (funcionario*) realloc(funcionarios, countVFuncionario * sizeof(funcionario)); //Realoca o vetor funcionarios com o novo tamanho.
	
	int i;
	for(i = (countVFuncionario-5); i < countVFuncionario; i++){
		funcionarios[i+1].id = -1; //Seta todos os ids para -1 para não ficar como 0
	}

	return funcionarios;
}

funcionario* inserirFuncionario(funcionario* funcionarios){

	int countFuncionarios = sizeofFuncionarios(funcionarios);

	if((countVFuncionario - countFuncionarios) == 0){
		funcionarios = realocarVFuncionario(funcionarios);	
	}

	char nome[20];
	int id;

	//Entrada de dados
	printf("Insira o nome: ");
	scanf("%s", nome);
	printf("Insira o id: ");
	scanf("%d", &id);

	//Verifica se já possui algum funcionário com este id, se sim reinicia o processo.
	int i;
	for(i = 0; i < countFuncionarios; i++){
		if(funcionarios[i].id == id){
			printf("\nEste id já está sendo utilizado por outro funcionário, tente novamente!\n\n");
			return inserirFuncionario(funcionarios); //Chamada recursiva, irá chamar novamente a função e irá retornar o resultado.
			break;
		}
	}

	//Se passou pelo teste então irá criar o novo funcionario.
	strcpy(funcionarios[countFuncionarios].nome, nome);
	funcionarios[countFuncionarios].id = id;

	return funcionarios;
}

funcionario* removerFuncionario(funcionario* funcionarios){	
	
	int id; 
	int pos = -1; //Posição do funcionario no vetor, inicia com -1 para o caso de não encontrar o funcionario.

	//Entrada de id para procurar o funcionario no vetor;
	printf("ID: ");
	scanf("%d", &id);

	//Irá procurar no vetor o nome e sobrenome digitado.
	int i;
	for(i = 0; i < sizeofFuncionarios(funcionarios); i++){
		if(funcionarios[i].id == id){
			pos = i;
			break;
		}
	}

	//Caso encontre o "pos" será > -1
	if(pos > -1){
		//Cria novo vetor para adicionar somente os que não foram excluidos;
		funcionario* funcionarios2 = alocarVFuncionario(countVFuncionario);

		//adiciona os funcionarios do vetor "funcionarios" no novo vetor "funcionarios2"
		int i;
		for(i = 0; i < (sizeofFuncionarios(funcionarios)-1); i++){
			if(i >= pos){
				funcionarios2[i] = funcionarios[i+1];
			}else{
				funcionarios2[i] = funcionarios[i];
			}
		}

		//É retornado o novo vetor já com o funcionario excluido.
		return funcionarios2;
	}else{
		//Funcionario não encontrado, então irá retornar o mesmo vetor sem alterações.
		printf("\nFuncionario não encontrado. Tente novamente.");
		return funcionarios;
	}
}

void imprimeFuncionario(funcionario* funcionarios){

	if(sizeofFuncionarios(funcionarios)==0){
		printf("\nNão há funcionarios cadastrados!!!\n\n");
		return;
	}

	printf("\n_________________________________________\n");
	printf("|QTD  | ID        | NOME                |\n");
	printf("|_____|___________|_____________________|\n");
  	//Percorre todo o vetor de funcionarios exibindo suas informações
	int i;
	for(i = 0; i < sizeofFuncionarios(funcionarios); i++){
		if(&funcionarios[i]){
			printf("| %-4d| %-10d| %-20s|", i+1, funcionarios[i].id, funcionarios[i].nome);
		}
	printf("\n|_____|___________|_____________________|\n");
	}
}

//Calcula o tamanho do vetor(Não calcula a memória alocada, calcula a quantidade de funcionarios incluidas no vetor).
int sizeofFuncionarios(funcionario* funcionarios){
	int count = 0;

	//Percorre todo o vetor até que encontre um espaço do vetor que não esteja ocupado por um fatura.(Que estejavazio)
	int i;
	for(i = 0; i < countVFuncionario; i++){
		if(funcionarios[count].id == -1){
			break;
		}
		count++;
	}
	
	return count;
}




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFuncionario(funcionario* funcionarios){
	criarArquivo(NOMEARQUIVOFUNCIONARIO);

	FILE* file = abrirArquivo(NOMEARQUIVOFUNCIONARIO);

	fseek ( file, 0, SEEK_SET);

	int i;
	for(i = 0; i < sizeofFuncionarios(funcionarios); i++){
		fwrite(&funcionarios[i], sizeof(funcionario), 1, file);
	}

	fclose(file);
}

funcionario* recuperarDadosFuncionario(){
	funcionario* funcionarios = alocarVFuncionario(countVFuncionario);

	FILE* file = abrirArquivo(NOMEARQUIVOFUNCIONARIO);

	if(file){
		fseek ( file, 0, SEEK_SET);
		int i;
		for(i = 0; i < countVFuncionario; i++){
			fread(&funcionarios[i], sizeof(funcionario), 1, file);
		}
		fclose(file);
	}

	return funcionarios;
}

void salvarContagemFuncionario(funcionario* funcionarios, FILE* file){	
	int position = 1;
  	int qtd = sizeofFuncionarios(funcionarios);
  	fseek ( file, position * sizeof(int), SEEK_SET);
    fwrite (&qtd, sizeof (int), 1, file);
}

void recuperarContagemFuncionario(FILE* file){
	int countRead = 0;
	int position = 1;
	fseek ( file, position * sizeof(int), SEEK_SET);

	fread (&countRead, sizeof(int), 1, file);
  	if(countRead != 0){
  		countVFuncionario = countRead;
  	}
	printf ("Funcionarios: %d\n", countVFuncionario);  
}

void removerArquivoFuncionario(){
	remove(NOMEARQUIVOFUNCIONARIO);
}