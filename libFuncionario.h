#define TAMINICIALFUNCIONARIO 5

typedef struct funcionario{
	char nome[20];
	int id;	
}funcionario;

funcionario* alocarVFuncionario(int tam);

funcionario* realocarVFuncionario(funcionario* funcionarios);

funcionario* inserirFuncionario(funcionario* funcionarios);

funcionario* removerFuncionario(funcionario* funcionarios);

void imprimeFuncionario(funcionario* funcionarios);

int sizeofFuncionarios(funcionario* funcionarios);




/*************************************************************************
*                	Salvar dados em arquivos                           *
*************************************************************************/

void salvarDadosFuncionario(funcionario* funcionarios);

funcionario* recuperarDadosFuncionario();

void salvarContagemFuncionario(funcionario* funcionarios, FILE* file);

void recuperarContagemFuncionario(FILE* file);

void removerArquivoFuncionario();